﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Register
    
    '''<summary>
    '''hdStudentID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdStudentID As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''hdRegisterID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdRegisterID As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''lblStdCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblStdCode As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblStudentName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblStudentName As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ddlYear control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlYear As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''optTerm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optTerm As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''txtRegDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRegDate As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''lblStatusName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblStatusName As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''hdTopicUID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdTopicUID As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''lblTopic control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTopic As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''pnSubject control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnSubject As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''ddlSubject control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlSubject As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''cmdAddSubject control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdAddSubject As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''grdSubject control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdSubject As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''lblTotalUnit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotalUnit As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''cmdSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdSave As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''cmdPrint control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdPrint As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''cmdDelete control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdDelete As Global.System.Web.UI.WebControls.Button
End Class
