﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class WorkOutSideList
    
    '''<summary>
    '''cmdNew control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdNew As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''ddlYear control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlYear As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''ddlWorkType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlWorkType As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''ddlGroup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlGroup As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''optIsPresented control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optIsPresented As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''ddlCountry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCountry As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''ddlBudget control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlBudget As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''txtSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSearch As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''cmdSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdSearch As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''cmdExport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdExport As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''cmdAdd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdAdd As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''UpdatePanel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''grdData control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdData As Global.System.Web.UI.WebControls.GridView
End Class
